/*
 * This is where the implementation of the shell starts
 * Authors:
 *  Adrian Albu <a.albu@vu.nl>
 */
 
#include "parser/ast.h"
#include "shell.h"
#include "stdio.h"
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int init_done = 0;

void handle_command(node_t *node, int *link, int cmd_nr, char **cmd)
{
    if (link != NULL)
    {
        int pid = fork();

        if (pid == -1)
        {
            perror("fork error");
        }
        else if (pid == 0)
        {
            if (cmd_nr == 1)
            {
                dup2(link[1], fileno(stdout));
            }
            else if (cmd_nr == 2)
            {
                dup2(link[0], fileno(stdin));
            }
            close(link[0]);
            close(link[1]);
            execvp(cmd[0], cmd);
        }
        else
        {
            initialize();
        }
    }
    else if (strcmp("exit", node->command.program) == 0) //built in
    {
        if (node->command.argc > 1)
        {
            exit(atoi(node->command.argv[1]));
        }
        else
        {
            exit(42);
        }
    }
    else if (strcmp("cd", node->command.program) == 0) //built in
    {
        if (node->command.argc > 1)
        {
            char cwd[1024];
            char *path = node->command.argv[1];
            int err;

            if (path[0] == '/')
            {
                err = chdir(path);
            }
            else
            {
                getcwd(cwd, sizeof(cwd));
                strcat(cwd, "/");
                strcat(cwd, path);
                err = chdir(cwd);
            }
            if (err < 0)
            {
                perror("No such file or directory \n");
            }
        }
        else
        {
            perror("No arguments given\n");
        }
    }
    else if (node->command.argc > 0)
    {
        int pid = fork();
        int child_status;

        if (pid == -1)
        {
            perror("fork error");
        }
        else if (pid == 0)
        {
            execvp(node->command.program, node->command.argv);
            perror("No binary found \n");
        }
        else
        {
            wait(&child_status);
            initialize();
        }
    }
    else
    {
        perror("Invalid command \n");
    }
}

void handle_pipe(node_t *node)
{
    int nr_of_commands = node->pipe.n_parts;
    if (nr_of_commands == 2)
    {
        int pipefd[2];
        pipe(pipefd);
        int *ptr = pipefd;
        node_t **parts = node->pipe.parts;
        node_t *part_1 = parts[0];
        node_t *part_2 = parts[1];
        handle_command(part_1, ptr, 1, part_1->command.argv);
        handle_command(part_2, ptr, 2, part_2->command.argv);
        close(pipefd[0]);
        close(pipefd[1]);
    }
}

void handle_sigint()
{
    //Do nothing about it
}

void initialize(void)
{
    /* This code will be called once at startup */
    if(init_done == 0)
    {
        signal(SIGINT, handle_sigint);
        init_done = 1;
    }

    if (prompt)
    {
        prompt = "vush$ ";
    }
}

void run_command(node_t *node)
{
    int pid, status;
    switch (node->type)
    {
    case NODE_COMMAND:
        handle_command(node, NULL, 1, NULL);
        break;
    case NODE_SEQUENCE:
        run_command(node->sequence.first);
        run_command(node->sequence.second);
        break;
    case NODE_PIPE:
        handle_pipe(node);
        break;
    case NODE_SUBSHELL:
        pid = fork();
        if (pid == 0)
        {
            run_command(node->subshell.child);
            exit(0);
        }
        else
        {
            wait(&status);
        }
        break;
    case NODE_DETACH:
        pid = fork();
        if (pid == 0)
        {
            run_command(node->detach.child);
            exit(0);
        }
        else
        {
            initialize();
        }
        break;
    default:
        break;
    }

    if (prompt)
    {
        prompt = "vush$ ";
    }
}
