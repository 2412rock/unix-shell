===================
Shell
===================

A Unix shell similar to sh, bash and zsh. 

#. Supports all built-in commands that can be found in most shells, such as: cd, mkdir, ls, sleep, exit etc.
#. Supports sequences of commands 
#. Suppors pipes of two simple commands
#. Supports interruptions of commands using Ctrl+C
#. SUpports executing commands in a sub-shell
#. Supports redirects